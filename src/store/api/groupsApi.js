import Vue from 'vue'
import api from './api'


const initialState = {
  groups: {},
  isFetchingGroups: false,
  fetchGroupsError: null,
  isFetchingGroup: false,
  fetchGroupError: null,
  isCreatingGroup: false,
  createGroupError: null,
  isUpdatingGroup: false,
  updateGroupError: null,
  isDestroyingGroup: false,
  destroyGroupError: null
}

const state = Vue.util.extend({}, initialState)

// getters
const getters = {
  listGroups(state) {
    return Object.values(state.groups).sort((a, b) => {
      return a.name.localeCompare(b.name)
    })
  },

  groupById(state) {
    return id => state.groups[id.toString()]
  },

  isLoading(state) {
    return state.isFetchingGroups ||
      state.isFetchingGroup ||
      state.isCreatingGroup ||
      state.isUpdatingGroup ||
      state.isDestroyingGroup
  }
}

// actions
const actions = {
  loadGroups({commit}) {

    return new Promise((resolve, reject) => {
      commit('LOAD_GROUPS_STARTED')
      api.get('/groups')
        .then((response) => {
          commit('LOAD_GROUPS_SUCCESS', response.body)
          resolve()
        })
        .catch((error) => {
          commit('LOAD_GROUPS_FAILURE', error)
          reject(error)
        })
    })
  },

  loadGroup({commit}, id) {
    return new Promise((resolve, reject) => {
      commit('LOAD_GROUP_STARTED')
      api.get('/groups/' + id)
        .then((response) => {
          commit('LOAD_GROUP_SUCCESS', response.body)
          resolve()
        })
        .catch((error) => {
          commit('LOAD_GROUP_FAILURE', error)
          reject(error)
        })
    })
  },

  createGroup({commit}, group) {
    return new Promise((resolve, reject) => {
      commit('CREATE_GROUP_STARTED')
      api.post('/groups/', group)
        .then((response) => {
          commit('CREATE_GROUP_SUCCESS', response.body)
          resolve()
        })
        .catch((error) => {
          commit('CREATE_GROUP_FAILURE', error)
          reject(error)
        })
    })
  },

  updateGroup({commit}, {id, group}) {
    return new Promise((resolve, reject) => {
      commit('UPDATE_GROUP_STARTED')
      api.put('/groups/' + id, group)
        .then((response) => {
          commit('UPDATE_GROUP_SUCCESS', response.body)
          resolve()
        })
        .catch((error) => {
          commit('UPDATE_GROUP_FAILURE', error)
          reject(error)
        })
    })
  },

  deleteGroup({commit}, id) {
    return new Promise((resolve, reject) => {
      commit('DELETE_GROUP_STARTED')
      api.delete('/groups/' + id)
        .then(() => {
          commit('DELETE_GROUP_SUCCESS', id)
          resolve()
        })
        .catch((error) => {
          commit('DELETE_GROUP_FAILURE', error)
          reject(error)
        })
    })
  }

}

// mutations
const mutations = {
  LOAD_GROUPS_STARTED(state) {
    state.groups = {}
    state.isFetchingGroups = true
    state.fetchGroupsError = null
  },
  LOAD_GROUPS_SUCCESS(state, groups) {
    groups.forEach((m) => {
      Vue.set(state.groups, m['_id'].toString(), m)
    })
    state.isFetchingGroups = false
    state.fetchGroupsError = null
  },
  LOAD_GROUPS_FAILURE(state, error) {
    state.isFetchingGroups = false
    state.fetchGroupsError = error
  },

  LOAD_GROUP_STARTED(state) {
    state.isFetchingGroup = true
    state.fetchGroupError = null
  },
  LOAD_GROUP_SUCCESS(state, group) {
    Vue.set(state.groups, group._id, group)
    state.isFetchingGroup = false
    state.fetchGroupError = null
  },
  LOAD_GROUP_FAILURE(state, error) {
    state.isFetchingGroup = false
    state.fetchGroupError = error
  },

  CREATE_GROUP_STARTED(state) {
    state.isCreatingGroup = true
    state.createGroupError = null
  },
  CREATE_GROUP_SUCCESS(state, group) {
    Vue.set(state.groups, group._id, group)
    state.isCreatingGroup = false
    state.createGroupError = null
  },
  CREATE_GROUP_FAILURE(state, error) {
    state.isCreatingGroup = false
    state.createGroupError = error
  },

  UPDATE_GROUP_STARTED(state) {
    state.isUpdatingGroup = true
    state.updateGroupError = null
  },
  UPDATE_GROUP_SUCCESS(state, group) {
    Vue.set(state.groups, group._id, group)
    state.isUpdatingGroup = false
    state.updateGroupError = null
  },
  UPDATE_GROUP_FAILURE(state, error) {
    state.isUpdatingGroup = false
    state.updateGroupError = error
  },

  DELETE_GROUP_STARTED(state) {
    state.isDestroyingGroup = true
    state.destroyGroupError = null
  },
  DELETE_GROUP_SUCCESS(state, id) {
    Vue.delete(state.groups, id.toString())
    state.isDestroyingGroup = false
    state.destroyGroupError = null
  },
  DELETE_GROUP_FAILURE(state, error) {
    state.isDestroyingGroup = false
    state.destroyGroupError = error
  },

  RESET(state) {
    for (let field in state) {
      Vue.set(state, field, initialState[field])
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
  namespaced: true,

}
