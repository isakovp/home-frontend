import Vue from 'vue'
import store from '../index'

const API_ENDPOINT = 'http://localhost:3003'

const auth = () => {
  return {
    'X-Access-Token': localStorage.getItem('token') || '',
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
}

export default {
  async get(url, request) {
    store.commit('API_ERROR', null, {root: true})
    return await Vue.http.get(API_ENDPOINT + url, {params: request, headers: auth()}).catch((error) => {
      store.commit('API_ERROR', error, {root: true})
      return Promise.reject(error)
    })
  },
  async post(url, request) {
    store.commit('API_ERROR', null, {root: true})
    return await Vue.http.post(API_ENDPOINT + url, request, {headers: auth()})
      .catch((error) => {
        store.commit('API_ERROR', error, {root: true})
        return Promise.reject(error)
      })
  },
  async put(url, request) {
    store.commit('API_ERROR', null, {root: true})
    return await Vue.http.put(API_ENDPOINT + url, request, {headers: auth()})
      .catch((error) => {
        store.commit('API_ERROR', error, {root: true})
        return Promise.reject(error)
      })
  },
  async delete(url, request) {
    store.commit('API_ERROR', null, {root: true})
    return await Vue.http.delete(API_ENDPOINT + url, {params: request, headers: auth()})
      .catch((error) => {
        store.commit('API_ERROR', error, {root: true})
        return Promise.reject(error)
      })
  }
}
