import Vue from 'vue'
import api from './api'


const initialState = {
  currentUser: {},
  isFetchingUser: false,
  fetchUserError: null,
  isUpdatingUser: false,
  updateUserError: null
}

const state = Vue.util.extend({}, initialState)

// getters
const getters = {

  isLoading(state) {
    return state.isFetchingUser ||
      state.isUpdatingUser
  }
}

// actions
const actions = {
  loadUser({commit}) {

    return new Promise((resolve, reject) => {
      commit('LOAD_USER_STARTED')
      api.get('/users/me')
        .then((response) => {
          commit('LOAD_USER_SUCCESS', response.body)
          resolve()
        })
        .catch((error) => {
          commit('LOAD_USER_FAILURE', error)
          reject(error)
        })
    })
  },

  updateUser({commit}, user) {
    return new Promise((resolve, reject) => {
      commit('UPDATE_USER_STARTED')
      api.put('/users/', user)
        .then((response) => {
          commit('UPDATE_USER_SUCCESS', response.body)
          resolve()
        })
        .catch((error) => {
          commit('UPDATE_USER_FAILURE', error)
          reject(error)
        })
    })
  }

}

// mutations
const mutations = {
  LOAD_USER_STARTED(state) {
    state.currentUser = {}
    state.isFetchingUser = true
    state.fetchUserError = null
  },
  LOAD_USER_SUCCESS(state, user) {
    state.currentUser = user
    state.isFetchingUser = false
    state.fetchUserError = null
  },
  LOAD_USER_FAILURE(state, error) {
    state.isFetchingUser = false
    state.fetchUserError = error
  },

  UPDATE_USER_STARTED(state) {
    state.isUpdatingUser = true
    state.updateUserError = null
  },
  UPDATE_USER_SUCCESS(state, user) {
    state.currentUser = user
    state.isUpdatingUser = false
    state.updateUserError = null
  },
  UPDATE_USER_FAILURE(state, error) {
    state.isUpdatingUser = false
    state.updateUserError = error
  },

  RESET(state) {
    for (let field in state) {
      Vue.set(state, field, initialState[field])
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
  namespaced: true,

}
