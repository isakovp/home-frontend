import api from './api'

const state = {
  token: localStorage.getItem('token') || '',
  status: '',
  currentUser: {}
}

// getters
const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status
}

// actions
const actions = {
  authenticate({commit}, form) {

    return new Promise((resolve, reject) => {
      commit('AUTH_REQUEST')
      api.post('/authentication', {username: form.username, password: form.password})
        .then((response) => {
          let token = response.body.apiToken
          localStorage.setItem('token', token)
          commit('AUTH_SUCCESS', token)
          resolve(token)
        })
        .catch((error) => {
          localStorage.removeItem('token')
          commit('AUTH_FAILURE', error)
          commit('API_ERROR', error, { root: true })
          reject(error)
        })
    })
  },

  logout({commit}) {
    return new Promise((resolve) => {
      localStorage.removeItem('token')
      commit('AUTH_LOGOUT')
      resolve()
    })
  },

  me({commit}) {
    return new Promise((resolve, reject) => {
      api.get('/users/me')
        .then((response) => {
          commit('CURRENT_USER', response.body)
          resolve(response.body)
        })
        .catch((error) => {
          commit('API_ERROR', error, { root: true })
          reject(error)
        })
    })
  }
}

// mutations
const mutations = {
  AUTH_REQUEST(state) {
    state.status = 'loading'
  },
  AUTH_SUCCESS(state, token) {
    state.status = 'success'
    state.token = token
  },
  AUTH_FAILURE(state) {
    state.status = 'error'
    state.currentUser = {}
  },

  AUTH_LOGOUT(state) {
    state.token = null
    state.currentUser = {}
  },

  CURRENT_USER(state, user) {
    state.currentUser = user
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
  namespaced: true,
}
