import Vue from 'vue'
import Vuex from 'vuex'
import authApi from './api/authApi'
import groupsApi from './api/groupsApi'
import usersApi from './api/usersApi'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  state: {
    apiError: null
  },
  modules: {
    authApi,
    groupsApi,
    usersApi
  },
  mutations: {
    API_ERROR(state, error) {
      state.apiError = error
    },
    RESET(state) {
      state.apiError = null
    }
  },
  actions: {
    resetState({commit}) {
      commit('RESET')
      commit('groupsApi/RESET')
      commit('usersApi/RESET')
    }
  }
})


