import Vue from 'vue'
import Router from 'vue-router'
import Home from './../components/Home'
import GroupsIndex from '../components/groups/Index'
import GroupsShow from '../components/groups/Show'
import Profile from '../components/profile/Index'
import SignIn from './../components/SignIn'
import PageNotFound from './../components/shared/PageNotFound'
import store from '../store/index'

Vue.use(Router)


const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['authApi/isAuthenticated']) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters['authApi/isAuthenticated']) {
    next()
    return
  }
  localStorage.setItem('next_url', to.name)
  next('/sign_in')
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/groups',
      name: 'groups',
      component: GroupsIndex,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/groups/:id',
      name: 'group',
      component: GroupsShow,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/sign_in',
      name: 'signIn',
      component: SignIn,
      beforeEnter: ifNotAuthenticated
    },
    {
      path: '/404',
      name: 'pageNotFound',
      component: PageNotFound
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})
